const app = new PIXI.Application({
	"width" : 1200,
	"height" : 1000,
	"view" : document.getElementById("game_canvas"),
	"backgroundColor" : 0x000000,
	"autoStart" : true
});

var graphics = new PIXI.Graphics();

graphics.lineStyle(1, 0x0000FF);
graphics.beginFill(0x0000FF);
graphics.drawCircle(0,0,45);
graphics.endFill();

graphics.lineStyle(1, 0xFF0000);
graphics.beginFill(0xFF0000);
graphics.drawRect(0,100,150,210);
graphics.endFill();

graphics.lineStyle(2, 0x00FF00);
graphics.beginFill(0x00FF00);
graphics.drawEllipse(50,400,180,75);
graphics.endFill();

graphics.lineStyle(1, 0x0000FF);
graphics.beginFill(0x000000);
graphics.drawCircle(150,0,45);
graphics.endFill();

graphics.lineStyle(1, 0xFF0000);
graphics.beginFill(0x000000);
graphics.drawRect(250,100,150,210);
graphics.endFill();

graphics.lineStyle(2, 0x00FF00);
graphics.beginFill(0x000000);
graphics.drawEllipse(450,400,180,75);
graphics.endFill();

graphics.lineStyle(3, 0xFFFFFF);
graphics.moveTo(300, 0);
graphics.lineTo(350, 0);
graphics.lineTo(350, 50);
graphics.lineTo(300, 50);
graphics.lineTo(300, 0);

graphics.lineStyle(3, 0xFFFFFF);
graphics.moveTo(600, 0);
graphics.lineTo(615, 40);
graphics.lineTo(645, 40);
graphics.lineTo(625, 80);
graphics.lineTo(635, 110);
graphics.lineTo(600, 90);
graphics.lineTo(565, 110);
graphics.lineTo(575, 80);
graphics.lineTo(555, 40);
graphics.lineTo(585, 40);
graphics.lineTo(600, 0);

graphics.lineStyle(2, 0x00FF00);
graphics.beginFill(0x00FF00);
graphics.drawEllipse(750,0,15,40);
graphics.endFill();

graphics.lineStyle(2, 0x00FF00);
graphics.beginFill(0x00FF00);
graphics.drawEllipse(800,0,15,40);
graphics.endFill();

graphics.lineStyle(2, 0xFFFF00);
graphics.beginFill(0xFFFF00);
graphics.drawCircle(775,75,45);
graphics.endFill();

graphics.lineStyle(2, 0xFF0000);
graphics.beginFill(0xFF0000);
graphics.drawCircle(760,65,5);
graphics.endFill();

graphics.lineStyle(2, 0xFF0000);
graphics.beginFill(0xFF0000);
graphics.drawCircle(790,65,5);
graphics.endFill();

graphics.lineStyle(4, 0x0000FF);
graphics.moveTo(750, 85);
graphics.lineTo(800, 85);

graphics.lineStyle(4, 0x0000FF);
graphics.beginFill(0xFFFFFF);
graphics.drawRect(760,84.5,12.5,18);
graphics.endFill();

graphics.lineStyle(4, 0x0000FF);
graphics.beginFill(0xFFFFFF);
graphics.drawRect(780,84.5,12.5,18);
graphics.endFill();


graphics.x = 150;
graphics.y = 150;

app.stage.addChild(graphics);